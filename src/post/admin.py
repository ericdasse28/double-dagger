from django.contrib import admin
from .models import Company, Post


# Register your models here.
class MyCompanyAdmin(admin.ModelAdmin):
    model = Company
    list_display = ('name', 'slug', 'description', 'pe_ratio',)


admin.site.register(Company, MyCompanyAdmin)


class MyPostAdmin(admin.ModelAdmin):
    model = Post
    list_display = ('title', 'slug', 'pub_date')


admin.site.register(Post, MyPostAdmin)
